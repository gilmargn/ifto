# IFTO - Campus Colinas do Tocantins 2017 - 2019
## Curso médio Integrado em Informática
![IFTO](ifto.png)


Neste repositório adicionei conteúdo, avaliações e exercícios ministrados no curso técnico em redes de computadores.

## Disciplinas Ensino médio técnico 

* Banco de dados
* Desenvolvimento de Sistemas
* Programação Orientado a Objetos
* Informática Aplicada a Software Livre
* Introdução a Redes de Computadores

## Disciplinas Licenciatura em Computação 

* Engenharia de software
* Inteligência Artificial(Introdução)
* Interface Humano computador

# Observação

O conteúdo disponibilizado foi utilizado em um **_curso técnico integrado ao ensino médio_** e na 
Licenciatura em Computação.(disciplinas e focos diferentes).

Esse material não substitui outros conteúdos incríveis na internet e não representa a totalidade que fora apresentado durante as aulas.






## Vídeos
[Os moderadores da Internet](https://www.youtube.com/watch?v=k9m0axUDpro)

[Construção de um avião durenta o voo](https://www.youtube.com/watch?v=UZq4sZz56qM)
